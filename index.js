import {createWeatherBlock, filterWeatherForThreeDays} from './weatherBlocks.js';

const weatherInMinskButton = document.querySelector('.weatherMinsk');
const localWeatherButton = document.querySelector('.localButton');
const buttonForThreeDays = document.querySelector('.buttonForThreeDays');
const spinner = document.querySelector('.spin');
const weatherBlock = document.querySelector('.weatherNow');
const closeButton = document.querySelector('.close');

class WeatherWidget{
    constructor(city){
        this.city = city;
    }

    //реализация с promise
    getWeather(){
        fetch(`http://api.openweathermap.org/data/2.5/weather?q=${this.city}&appid=064b080ba3d47f98e027c97f723b9a46`).then(resp => {
            return resp.json()
        }).then(resp => {
            createWeatherBlock(resp)
        }).catch(e => console.log('error'))
    }
    
    // реализация с async/await
    // async getWeather(){
    //     const request = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${this.city}&appid=064b080ba3d47f98e027c97f723b9a46`);
    //     request.json().then(resp => {
    //         createWeatherBlock(resp)
    //     })
    // }

    //реализация с callBack
    // async getWeather(callback){
    //     const request = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${this.city}&appid=064b080ba3d47f98e027c97f723b9a46`)
    //     request.json().then(resp => {
    //             callback(resp)
    //         })
    // }
}

//инициализация объекта погоды
const weatherWidgetMinsk = new WeatherWidget('minsk');
weatherWidgetMinsk.getWeather();

//инициализация объекта погоды при использовании колбэка
// weatherWidgetMinsk.getWeather(createWeatherBlock)


//если повторно нужно будет посмотреть погоду в Минске
weatherInMinskButton.addEventListener('click', () => {
    weatherWidgetMinsk.getWeather();
    weatherBlock.style.display = 'block';
});


// погода по локации
class LocalWeather extends WeatherWidget{
    constructor(city){
        super(city);
    }

    getLocalWeather(lat, lon){
        fetch(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=064b080ba3d47f98e027c97f723b9a46`).then(resp => {
            return resp.json()
        }).then(resp => {
            createWeatherBlock(resp);
            weatherBlock.style.display = 'block';
        }).catch(e => console.log('error'))
    }
}

const localWeather = new LocalWeather('minsk');
localWeatherButton.addEventListener('click', () => {
    function success(position){
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;
        localWeather.getLocalWeather(latitude, longitude);
    }
    function error(){
        console.log('error')
    }
    navigator.geolocation.getCurrentPosition(success, error);
})

//погода на 3 дня
class ThreeDaysWeather extends WeatherWidget{
    constructor(city){
        super(city)
    }

    getWeatherForThreeDays(){
        fetch(`http://api.openweathermap.org/data/2.5/forecast?q=${this.city}&appid=064b080ba3d47f98e027c97f723b9a46`).then(resp => {
            return resp.json()
        }).then(resp => {
            filterWeatherForThreeDays(resp);
        }).catch(e => console.log('error'))
    }
}

const threeDaysWeather = new ThreeDaysWeather('minsk');
buttonForThreeDays.addEventListener('click', () => {
    if(document.querySelectorAll('.day').length < 3){
        spinner.style.display = 'block';
    }
    threeDaysWeather.getWeatherForThreeDays();
})

//закрыть окно с погодой сейчас
closeButton.addEventListener('click', () => {
    weatherBlock.style.display = 'none';
})