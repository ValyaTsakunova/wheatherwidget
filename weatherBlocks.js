const city = document.querySelector('.city');
const weatherIcon = document.querySelector('img');
const degree = document.querySelector('.degree');
const description = document.querySelector('.description')
const windSpeed = document.querySelector('.wind');
const weatherForThreeDaysBlock = document.querySelector('.weatherForThreeDays');
const spinner = document.querySelector('.spin');

//создаем блок с погодой
export function createWeatherBlock(data){
    const {weather, name, main, wind} = data;
    city.textContent = `Now in ${name}`;
    degree.textContent = `${Math.round(parseFloat(main.temp)-273.15)}°C`;
    weatherIcon.src = `http://openweathermap.org/img/w/${weather[0].icon}.png` ;
    description.textContent = `${weather[0].description}`;
    windSpeed.textContent = `Wind: ${wind.speed} m/s`;
}

//фильтруем массив с погодой за 5 дней
export function filterWeatherForThreeDays (info){
    const {list} = info;
    list.slice(0, 24).map(item => {
        if(item.dt_txt.includes('12:00:00')){
            createBlockForOneDay(item)
        }
    })
}

//формируем блок с погодой за один день 
function createBlockForOneDay(item){
    const {dt_txt, main, weather, wind} = item;
    const elem = `<div class="day">
                    <h1 >${dt_txt.slice(5,7)}.${dt_txt.slice(8,10)}</h1>
                    <div class="degree">${Math.round(parseFloat(main.temp)-273.15)}°C</div>
                    <div ><img src="http://openweathermap.org/img/w/${weather[0].icon}.png" alt=""></div>
                    <div class="description">${weather[0].description}</div>
                    <div class="wind">Wind: ${wind.speed} m/s</div>
                </div>`;
    if(document.querySelectorAll('.day').length < 3){
        weatherForThreeDaysBlock.insertAdjacentHTML('beforeend', elem);
        spinner.style.display = 'none';
    }
}